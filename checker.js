const util = require("util"),
    fetch = require("node-fetch")
    md5 = require("md5"),
    http = require("http");


const [,,...args] = process.argv;

if (!args[0] || !args[1]) {
    console.log('try running "node checker.js <website> <md5 hash>" instead');
    process.exit();
}

async function check(){
    const response = await fetch(args[0]);
    const results = await response.text();
    const hash = md5(results);

    if (hash.normalize().trim().toLowerCase() !== args[1].normalize().trim().toLowerCase()) {
        console.log(`website changed ${args[0]}`);
        console.log('\u0007');

        if (args[2]) {
           console.log(results);
        }
    } else {
        console.log('nope');
    }
}

check();
setInterval(check, 5000);


