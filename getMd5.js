const util = require("util"),
    fetch = require("node-fetch")
    md5 = require("md5"),
    http = require("http");


const [,,...args] = process.argv;

if (!args[0]) {
    console.log('try running "node getMd5.js <website>" instead');
    process.exit();
}

async function check(){
    const response = await fetch(args[0]);
    const results = await response.text();

    console.log(md5(results));
}

check();
